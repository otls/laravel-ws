<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $wsURL = config('ws.host') == "0.0.0.0" ? "127.0.0.1" : config('ws.host');
    $wsPort = config('ws.port');
    $ws = $wsURL . ":" . $wsPort;
    return view('welcome', compact('ws'));
});
