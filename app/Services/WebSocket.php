<?php

namespace App\Services;

use Ratchet\Server\IoServer;
use App\Http\Controllers\WebsocketController;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

class WebSocket
{
    public function run()
    {
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new WebsocketController()
                )
            ),
            config('ws.port'),
            config('ws.host')
        );

        return $server->run();
    }
}
