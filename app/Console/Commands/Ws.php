<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\WebSocket;

class Ws extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ws:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test WS ratchet';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $service = new WebSocket();
        return $service->run();
    }
}
