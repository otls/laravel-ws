<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ipddns extends Model
{
    use HasFactory;
    protected $fillable = ['ip'];
}
