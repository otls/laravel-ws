<?php

return [
    'host' => env('WS_URL', '0.0.0.0'),
    'port' => env('WS_PORT', 8080)
];
