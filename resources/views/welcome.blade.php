<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel WS</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css" integrity="sha512-P5MgMn1jBN01asBgU0z60Qk4QxiXo86+wlFahKrsQf37c9cro517WzVSPPV1tDKzhku2iJ2FVgL67wG03SGnNA==" crossorigin="anonymous" />

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js" integrity="sha512-XKa9Hemdy1Ui3KSGgJdgMyYlUg1gM+QhL6cnlyTe2qzMCYm4nAZ1PsVerQzTTXzonUR+dmswHqgJPuwCq1MaAg==" crossorigin="anonymous"></script>

    </head>
    <body class="my-2 text-sm">
        <div class="container p-0">
            <div class="card" style="height: 95vh">
                <div class="card-header">
                    <h3 class="h4 card-text">Laravel Test Chat</h3>
                </div>
                <div class="card-body overflow-auto scroll" id="chat-body">

                </div>
                <div class="card-footer bg-white">
                    <form action="" id="message-form">
                        <div class="form-row">
                            <div class="form-group col-md-10">
                                <input autocomplete="off" name="message-box" id="message-box" placeholder="message.." cols="10" rows="1" class="form-control" />
                            </div>
                            <div class="form-group col-md-2">
                                <button class="btn btn-block btn-primary" id="message-btn">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <script>
        let conn = null;
        senderBoxes = null;
        senderId = null;

        function generateMessageBox(sender, message) {
            senderBoxes = $(`.${sender}`)
            senderBoxId = `${sender}-${senderBoxes.length}`;

            return `
                <div id="${senderBoxId}" class="card shadow-sm border-sm my-2 ${sender}">
                    <div class="card-body">
                        <h5 class="h6 my-0 sender-section">Sender: ${sender}</h5>
                        <p class="text-sm my-0 message-section">${message}</p>
                    </div>
                </div>
            `;
        }

        function onMessageHanlder(sender, message) {
            let element = generateMessageBox(sender, message);

            $('#chat-body').append(element);

            element = document.getElementById(senderBoxId);
            element.scrollIntoView(false)
        }

        $(() => {
            let wsHost = `{{ $ws }}`;
            conn = new WebSocket(`ws:${wsHost}`);

            conn.onopen = function (e) {
                console.log("Connection established!");
            };

            conn.onmessage = function (e) {
                let data = JSON.parse(e.data);
                console.log('receiving');
                console.log(data);
                onMessageHanlder(data.sender, data.message);
            };
        });

        $(document).on('submit', '#message-form', (e) => {
            e.preventDefault();
            let message = $('#message-box').val();

            if (message && message.length > 0) {
                let data = {
                    message: message
                }
                data = JSON.stringify(data)
                console.log("Sending");
                console.log(data);
                conn.send(data);
            }

            $(e.currentTarget).trigger('reset');
        });
    </script>
</html>
